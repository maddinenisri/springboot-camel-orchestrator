package com.mdstech.sample.config;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class CustomRouteBuilder extends RouteBuilder {
    @Override
    public void configure() throws Exception {

        restConfiguration()
                .component("servlet")
                .bindingMode(RestBindingMode.json);

        rest().get("/process")
                .consumes(MediaType.APPLICATION_JSON_VALUE)
                .produces(MediaType.APPLICATION_JSON_VALUE)
                .type(JsonNode.class)
                .to("direct:processPayload");

        from("direct:processPayload")
                .log(LoggingLevel.INFO, bodyAs(Map.class).toString())
                .multicast()
                .parallelProcessing()
                .to( "seda:processAsync", "direct:processSync");

        from("seda:processAsync")
                .log(LoggingLevel.INFO, body().toString())
                .process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        Object object = exchange.getIn().getBody();
                        exchange.getIn().setHeader(Exchange.HTTP_METHOD, constant(HttpMethod.POST));
                        exchange.getIn().setHeader(Exchange.CONTENT_TYPE, MediaType.APPLICATION_JSON);
                        ObjectMapper objectMapper = new ObjectMapper();
                        exchange.getIn().setBody(objectMapper.writeValueAsString(object));
                    }
                })
                .to("http4://localhost:8080/api/v1/async?bridgeEndpoint=true")
        .end();


        from("direct:processSync").process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        JsonNode jsonNode = (JsonNode) exchange.getIn().getBody();
                        ((ObjectNode)jsonNode).put("test", "sample");
                        exchange.getOut().setBody(jsonNode.toString());
                    }
                })
                .to("jolt:./sample_spec.json?inputType=JsonString")
                .process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        Object object = exchange.getIn().getBody();
                        if(object instanceof Map) {
                            ((Map)object).put("type", "Map");
                        }
                        exchange.getIn().setHeader(Exchange.HTTP_METHOD, constant(HttpMethod.POST));
                        exchange.getIn().setHeader(Exchange.CONTENT_TYPE, MediaType.APPLICATION_JSON);
                        ObjectMapper objectMapper = new ObjectMapper();
                        exchange.getIn().setBody(objectMapper.writeValueAsString(object));
                    }
                })
                .to("http4://localhost:8080/api/v1/sync")
                .unmarshal().json(JsonLibrary.Jackson)
                .multicast()
                .parallelProcessing()
                .to("seda:processAsync", "direct:responseSync");

        from("direct:responseSync").transform().body();

    }
}