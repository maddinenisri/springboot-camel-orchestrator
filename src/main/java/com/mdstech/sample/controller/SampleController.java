package com.mdstech.sample.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

@RestController
public class SampleController {

    @PostMapping("/api/v1/sync")
    public ResponseEntity<Map> processRequest(@RequestBody Map<String, String> payload) {
        System.out.println(LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME)+ "....." + payload.toString());
        payload.put("processed", "true");
        return new ResponseEntity<>(payload, HttpStatus.OK);
    }

    @PostMapping("/api/v1/async")
    public ResponseEntity<Void> asyncProcess(@RequestBody Map<String, String> payload) {
        System.out.println(LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME)+ "+++++" + payload.toString());
        return new ResponseEntity(HttpStatus.OK);
    }
}
